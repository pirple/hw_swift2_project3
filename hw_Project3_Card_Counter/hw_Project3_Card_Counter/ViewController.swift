//
//  ViewController.swift
//  hw_Project3_Card_Counter
//
//  Created by Gilbert  on 5/25/19.
//  Copyright © 2019 Hop. All rights reserved.
//


/*
    For this assignment, you are to build a "card counter" app to help gamblers win at blackjack. Your app should allow the gambler to enter what cards are being dealt, as they happen, and should guide them as to how to bet and play, accordingly. Specifically they should be able to:

    1. Mark that a new deck is being opened/used by the dealer

    2. Enter which cards have been drawn, hand by hand

    3. Receive guidance from the app on whether to "hit" or "stay" at any given moment.

    4. Receive guidance from the app regarding how large of a bet they should be placing at any given time (use the system explained in the youtube video above to guide your development).
*/



    //
    // How many decks are in rotation?
    // how many players are at the table?
    // deck array of 4 which will

    // Don't forget the dealer. Dealers does not count as player.
    // Show the current count:
    // label to adivised to which players should hit and stay
    // Label to be more when the count is high    (buy in bet, med level bet, high bet)
    // New deck in rotation? or a new shuffle of all decks? then reset count
    //








import UIKit

class ViewController: UIViewController {
    // create deck --------------------------------> DONE
    // add a deck to existing deck function
    // add amount of decks for init(amoutnOfDecks)
    // search for index of entered cards



    // new the following functions for DeckOfCards class: Create deck using init (init(_ amountOfDecks) ),
    // shuffle deck, remove cards from deck, current count, NextMove: (hit or stay, which level of bet), add a new deck to the game,
    class DeckOfCards {
    
        var deck  : [(String, String, Int)] = [];
        let Suit  : [String] = ["Clubs", "Diamonds", "Hearts", "Spades"];
//        let Rank  : [String] = ["Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"];
        let Rank  : [String] = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"];
        let Score : [Int] = [-1, 1, 1, 1, 1, 1, 0, 0, 0, -1, -1, -1, -1];
        var runningCount : Int = 0;
        var players : Int = 1;

        init(){
            self.CreateDecks();
        }
        
        init(_ amountOfDecks : Int){
            if (amountOfDecks < 1) {return}
            for _ in 0..<amountOfDecks {
                CreateDecks();
            }
        }
        
        func CreateDecks(){
            for indexSuit in 0..<self.Suit.count {
                for indexRank in 0..<self.Rank.count {
                    self.deck.append((self.Suit[indexSuit], self.Rank[indexRank], self.Score[indexRank]))
                }
            }
        }
        
        
        func getScore(_ rank : String ) -> Int {
            for index in 0..<self.deck.count {
                    if (self.Rank[index] == (rank)) {
                        let newScore = self.Score[index];
                        return newScore;
                    }
                }
                return 999;
        }
        
        
        // remove a card
        func RemoveCard(_ suit : String, _ rank : String, _ score : Int){
            if self.deck.filter({ $0 == (suit, rank, score) }).first != nil {
                for index in 0..<self.deck.count {
                    if (self.deck[index] == (suit, rank, score)) {
                        self.deck.remove(at: index)
                        //update count:
                        self.OneCardCount((suit, rank, score));
                        return;
                    }
                }
            } else {
                // if there is not card in the deck that matches the input
                print("Card Not Found in deck. Please enter a new Card")
            }
        }
        
        // counts total players card, Need to run for each player  /// update runningCount
        func TwoCardsCount (_ playersFirstCard : (String, String, Int), _ playersSecondCard: (String, String, Int) ){
            let handCount = playersFirstCard.2 + playersSecondCard.2;
            self.runningCount += handCount;
        }
        
        // for players that hit
        func OneCardCount (_ OneCard : (String, String, Int)) {
            self.runningCount += OneCard.2;
        }
        
        
        // get the true count
        func getTrueCount() -> Int {
            print("self.deck.cont:   \(self.deck.count)")
            var remainingDecks : Double = Double((self.deck.count) / 52) + 1;
            print("Remaing Decks letf In Play: \(remainingDecks)")
            remainingDecks = (remainingDecks*2).rounded() / 2; // round to .5
            if self.deck.count <= 25  {remainingDecks = 0.5;}
            else if self.deck.count <= 52 {remainingDecks = 1.0;}
            let trueCount : Int = self.runningCount / Int(remainingDecks);
            return trueCount;
        }
        
        
        
        
        
        
        
    // end of class
    }

    var userSession : DeckOfCards = DeckOfCards();
    var bettingUnit : Int = 10;
    var playerHandArray : [(String, String, String, String)] = [];
    
    // labels
    // Decks In Play incrementer
    @IBOutlet weak var decksInPlayLabel: UILabel!
    @IBOutlet weak var startCountingLabel: UIButton!
    
    @IBOutlet weak var howManyDecksField: UITextField!
    @IBOutlet weak var bettingUnitField: UITextField!
    @IBOutlet weak var bettingUnitLabel: UILabel!
    @IBOutlet weak var howManyPlayersField: UITextField!
    
    
    @IBOutlet weak var suitPickerLabel: UISegmentedControl!
    @IBOutlet weak var rankPickerLabel: UISegmentedControl!
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var suitLabel: UILabel!
    
    @IBOutlet weak var oneOrTwoCardSelectionOutlet: UISegmentedControl!
    @IBOutlet weak var suitPickerLabel2: UISegmentedControl!
    @IBOutlet weak var rankPickerLabel2: UISegmentedControl!
    @IBOutlet weak var suitLabel2: UILabel!
    @IBOutlet weak var rankLabel2: UILabel!
    @IBOutlet weak var oneTwoCardSegmentSelection: UISegmentedControl!
    @IBOutlet weak var howManyCardMessageLabel: UILabel!
    
    
    @IBOutlet weak var playersTurnLabel: UILabel!
    @IBOutlet weak var playersSuggestionLabel: UILabel!
    @IBOutlet weak var submitButtonOutlet: UIButton!
    @IBOutlet weak var runningCountLabel: UILabel!
    @IBOutlet weak var trueCountLabel: UILabel!
    @IBOutlet weak var whichPlayersTurnLabel: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.removeSelections();
        
    }
    
    func removeSelections(){
        self.suitPickerLabel.removeAllSegments();
        self.rankPickerLabel.removeAllSegments();
        self.rankLabel.isHidden = true;
        self.suitLabel.isHidden = true;
        self.suitPickerLabel2.removeAllSegments();
        self.rankPickerLabel2.removeAllSegments();
        self.rankLabel2.isHidden = true;
        self.suitLabel2.isHidden = true;
        self.oneOrTwoCardSelectionOutlet.isHidden = true;
        self.oneTwoCardSegmentSelection.isHidden = true;
        self.howManyCardMessageLabel.isHidden = true;
        self.playersTurnLabel.isHidden = true;
        self.playersSuggestionLabel.isHidden = true;
        self.submitButtonOutlet.isHidden = true;
//        self.whichPlayersTurnLabel.isHidden = true;
        
    }
    
    
    
    
    
    

    @IBAction func startCountingButton(_ sender: UIButton) {
        var localDecks : Int = Int(self.howManyDecksField.text!) ?? 3;
        let localUnit : Int = Int(self.bettingUnitField.text!) ?? 10;
        var localPlayers : Int = Int(self.howManyPlayersField.text!) ?? 1;
        self.bettingUnit = localUnit;
        if(self.howManyDecksField.text == ""){
            localDecks = 3;
        }
        if(self.howManyPlayersField.text == ""){
            localPlayers = 1;
        }
        self.oneOrTwoCardSelectionOutlet.selectedSegmentIndex = 1;
        print("localDecks \(localDecks)")
        self.howManyDecksField.text = String(localDecks);
        self.bettingUnitField.text = String(localUnit);
        self.bettingUnitLabel.text = String(localUnit);
        self.howManyPlayersField.text = String(localPlayers);
        self.userSession = DeckOfCards(localDecks)
        print(self.userSession.deck)
        self.startCountingLabel.isHidden = true;
        self.bettingUnitField.isEnabled = false;
        self.howManyDecksField.isEnabled = false;
        self.howManyPlayersField.isEnabled = false;
        
        print(self.userSession.deck.count/52)
        for num in self.userSession.Suit.indices {
            self.suitPickerLabel.insertSegment(withTitle: self.userSession.Suit[num], at: num, animated: true)
        }
        for num in self.userSession.Rank.indices {
            self.rankPickerLabel.insertSegment(withTitle: self.userSession.Rank[num], at: num, animated: true)
        }
        for num in self.userSession.Suit.indices {
            self.suitPickerLabel2.insertSegment(withTitle: self.userSession.Suit[num], at: num, animated: true)
        }
        for num in self.userSession.Rank.indices {
            self.rankPickerLabel2.insertSegment(withTitle: self.userSession.Rank[num], at: num, animated: true);
        }
        self.suitPickerLabel.selectedSegmentIndex = 0;
        self.rankPickerLabel.selectedSegmentIndex = 0;
        self.rankLabel.isHidden = false;
        self.suitLabel.isHidden = false;
        self.suitPickerLabel2.selectedSegmentIndex = 0;
        self.rankPickerLabel2.selectedSegmentIndex = 0;
        self.rankLabel2.isHidden = false;
        self.suitLabel2.isHidden = false;
        self.oneOrTwoCardSelectionOutlet.isHidden = false;
        self.oneTwoCardSegmentSelection.isHidden = false;
        self.howManyCardMessageLabel.isHidden = false;
        self.playersTurnLabel.isHidden = false;
        self.playersSuggestionLabel.isHidden = false;
        self.submitButtonOutlet.isHidden = false;
//        self.whichPlayersTurnLabel.isHidden = false;
        // get betting unit 1 betting unit = ( true count - 1 ) * betting unit
    }
    
  

    @IBAction func oneOrTwoCardSelectionn(_ sender: UISegmentedControl) {
        
        if (self.oneOrTwoCardSelectionOutlet.selectedSegmentIndex == 0){
            self.suitPickerLabel2.removeAllSegments();
            self.rankPickerLabel2.removeAllSegments();
            self.rankLabel2.isHidden = true;
            self.suitLabel2.isHidden = true;
        } else {
            for num in self.userSession.Suit.indices {
            self.suitPickerLabel2.insertSegment(withTitle: self.userSession.Suit[num], at: num, animated: true)
            }
            for num in self.userSession.Rank.indices {
                self.rankPickerLabel2.insertSegment(withTitle: self.userSession.Rank[num], at: num, animated: true)
            }
            self.suitPickerLabel2.selectedSegmentIndex = 0;
            self.rankPickerLabel2.selectedSegmentIndex = 0;
            self.rankLabel2.isHidden = false;
            self.suitLabel2.isHidden = false;
        }
    }
    
    @IBAction func sumbitCardsButton(_ sender: UIButton) {
        // push cards to player
        // call remove cards function from class
        if (self.oneOrTwoCardSelectionOutlet.selectedSegmentIndex == 0){
            self.submitOneCard()
        } else {
            self.submitTwoCards();
        }
        let localTrueCount : Int = self.userSession.getTrueCount();
        self.trueCountLabel.text = String(localTrueCount);
        self.bettingUnitUpdate();
        // update running count and true count
        // check how many players are remaining, if there are players remaining update playersTurnLabel and maybe playersSuggestLabel
        // if no more players, call dealer function to enter their one card
    
        self.suitPickerLabel2.selectedSegmentIndex = 0;
        self.rankPickerLabel2.selectedSegmentIndex = 0;
        self.suitPickerLabel.selectedSegmentIndex = 0;
        self.rankPickerLabel.selectedSegmentIndex = 0;
        
    }
    
    func submitOneCard(){
        let card1Suit : String = userSession.Suit[self.suitPickerLabel.selectedSegmentIndex];
        let card1Rank : String = userSession.Rank[self.rankPickerLabel.selectedSegmentIndex];
        let card1Score : Int = userSession.getScore(card1Rank);
        self.userSession.RemoveCard(card1Suit, card1Rank, card1Score);
        self.runningCountLabel.text = String(userSession.runningCount);
    }
    
    
    // entering two cards
    func submitTwoCards(){
        let card1Suit : String = userSession.Suit[self.suitPickerLabel.selectedSegmentIndex];
        let card1Rank : String = userSession.Rank[self.rankPickerLabel.selectedSegmentIndex];
        let card2Suit : String = userSession.Suit[self.suitPickerLabel2.selectedSegmentIndex];
        let card2Rank : String = userSession.Rank[self.rankPickerLabel2.selectedSegmentIndex];
        let card1Score : Int = userSession.getScore(card1Rank);
        let card2Score : Int = userSession.getScore(card2Rank);
        self.playerHandArray.append((card1Suit, card1Rank, card2Suit, card2Rank));
        self.userSession.RemoveCard(card1Suit, card1Rank, card1Score);
        self.userSession.RemoveCard(card2Suit, card2Rank, card2Score);
        self.runningCountLabel.text = String(userSession.runningCount);
        self.nextMove();
        self.oneTwoCardSegmentSelection.selectedSegmentIndex = 0;
        self.oneTwoCardSegmentSelection.selectedSegmentIndex = 1;
    }
    

    func bettingUnitUpdate(){
        print("true count label : \(self.trueCountLabel.text)")
        let count : Int = Int(self.trueCountLabel.text!) ?? 1;
        print("count \(count)")
        let bettingUnit : Int = Int(self.bettingUnitField.text!) ?? 10;
        print("bettingUnit \(bettingUnit)");
        if (count <= 1){
            self.bettingUnitLabel.text = String(bettingUnit);
        } else {
            let newbettingUnit : Int = count * bettingUnit;
            print("nenwettingUnit : \(newbettingUnit)")
            self.bettingUnitLabel.text = String(newbettingUnit);
        }
    }

    
    func nextMove() {
        let card1Rank : String = userSession.Rank[self.rankPickerLabel.selectedSegmentIndex];
        let card2Rank : String = userSession.Rank[self.rankPickerLabel2.selectedSegmentIndex];
        
        let playerHandScore = self.calculateCards(card1Rank, card2Rank);
        
        if (Int(self.runningCountLabel.text!)! > 10 && playerHandScore <= 15) {
            self.playersSuggestionLabel.text = "Player has a score of \(playerHandScore). You should hit";
        }
        else if (Int(self.runningCountLabel.text!)! >= -2 && playerHandScore <= 15) {
            self.playersSuggestionLabel.text = "Player has a score of \(playerHandScore). You should hit";
        }
        else if (Int(self.runningCountLabel.text!)! <= -2 && playerHandScore <= 11) {
            self.playersSuggestionLabel.text = "Player has a score of \(playerHandScore). You should hit";
        } else {
            self.playersSuggestionLabel.text = "Player has a score of \(playerHandScore). You should Stay";
        }
    }


    func calculateCards(_ card1 : String, _ card2: String) -> Int {
        var playerScore1 : Int = 0;
        var playerScore2 : Int = 0;
        
        print("card1 : \(card1)")
        print("Card2 : \(card2)")
        
        switch card1 {
            case "K", "Q", "J", "k", "q", "j" :
                playerScore1 = 10;
            case "A", "a" :
                playerScore1 = 11;
            default:
                playerScore1 = Int(card1) ?? 0;
        }
        
        switch card2 {
            case "K", "Q", "J", "k", "q", "j" :
                playerScore2 = 10;
            case "A", "a" :
                playerScore2 = 11;
            default:
                playerScore2 = Int(card2) ?? 0;
        }
        
        
        
        return playerScore1 + playerScore2;
    }


}

